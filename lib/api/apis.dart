import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:task_manager_app/models/account.dart';
import 'package:task_manager_app/models/log.dart';
import 'package:task_manager_app/models/logrequest.dart';
import 'package:task_manager_app/models/ticket.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';

//Api Login
Future<bool> createProduct(Account a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/login");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  if(response.statusCode == 200){
    // Map<String, dynamic> data = json.decode(response.body);
    // String password = data['id'];
    // print('id: $password');
    return true;
  }
  else{
    return false;
  }
}

//Api get Ticket by UserId
Future<List<TaskModel>> fetchTicket() async{
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/ticket/UserId?phone=1'));
  if(response.statusCode == 200){
    print('Chạy api lay Ticket Thành Công');
    return compute(parseTicket, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

List<TaskModel> parseTicket(String responseBody){
  print('Chạy api parseTicket');
  var list = jsonDecode(responseBody) as List<dynamic>;
  print('Chạy api parseTicket 2');
  List<TaskModel> ticket = list.map((model) => TaskModel.fromJson(model)).toList();
  print('Chạy api parseTicket 3');
  return ticket;
}

//Api get Log By TicketID
Future<List<Log>> fetchLog(int id) async{
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/log/ticketId?ticketId=${id}'));
  if(response.statusCode == 200){
    print('Chạy api lay Log Thành Công');
    return compute(parseLog, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

List<Log> parseLog(String responseBody){
  print('Chạy api parseTicket');
  var list = jsonDecode(responseBody) as List<dynamic>;
  print('Chạy api parseTicket 2');
  List<Log> ticket = list.map((model) => Log.fromJson(model)).toList();
  print('Chạy api parseTicket 3');
  return ticket;
}

//Api thay đổi status
Future<bool> changeStatus(Ticket a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/api/ticket");
  final response = await http.put(uri, body: jsonEncode(a.toJson()), headers: headers );
  print(jsonEncode(a.toJson()));
  if(response.statusCode == 200){
    print("Login Success");
    return true;
  }
  else{
    return false;
  }
}

//Tạo Logs
Future<bool> createLog(LogRequest a) async {
  print('Có chạy createlog');
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/log");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  print(jsonEncode(a.toJson()));
  if(response.statusCode == 200){
    print("Login Success");
    return true;
  }
  else{
    return false;
  }
}