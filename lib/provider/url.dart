import 'package:flutter/foundation.dart';

class UrlProvider extends ChangeNotifier{
  String url ='';

  Future saveUrl(String a) async {
    List<String> parts = a.split("images/");
    String trimmedURL = parts.length > 1 ? parts[1] : a;
    url = trimmedURL;
    notifyListeners();
  }
}
