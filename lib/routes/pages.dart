class Pages {
  static const initial = '/';
  static const home = '/home';
  static const createNewTask = '/createNewTask';
  static const updateTask = '/updateNewTask';
  static const login = "/login";
  static const report = "/report";
  static const detail = "/detail";
  static const profile = "/profile";
}
