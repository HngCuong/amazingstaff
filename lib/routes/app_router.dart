import 'package:flutter/material.dart';
import 'package:task_manager_app/routes/pages.dart';
import 'package:task_manager_app/splash_screen.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';
import 'package:task_manager_app/tasks/presentation/pages/detail_ticket.dart';
import 'package:task_manager_app/tasks/presentation/pages/profile.dart';
import 'package:task_manager_app/tasks/presentation/pages/reportscreen.dart';
import 'package:task_manager_app/tasks/presentation/pages/signin_screen.dart';
import 'package:task_manager_app/tasks/presentation/pages/tasks_screen.dart';
import '../page_not_found.dart';

Route onGenerateRoute(RouteSettings routeSettings) {
  switch (routeSettings.name) {
    case Pages.initial:
      return MaterialPageRoute(
        builder: (context) => const SplashScreen(),
      );
    case Pages.home:
      return MaterialPageRoute(
        builder: (context) => const TasksScreen(),
      );
    case Pages.login:
      return MaterialPageRoute(
        builder: (context) => const SignInScreen(),
      );
    case Pages.report:
      final args = routeSettings.arguments as int;
      return MaterialPageRoute(
        builder: (context) => ReportScreen(id: args),
      );
    case Pages.detail:
      final args = routeSettings.arguments as TaskModel;
      return MaterialPageRoute(
        builder: (context) => DetailScreen(taskModel: args),
      );
    case Pages.profile:
      return MaterialPageRoute(
        builder: (context) => const ProfileScreen(),
      );
    default:
      return MaterialPageRoute(
        builder: (context) => const PageNotFound(),
      );
  }
}
