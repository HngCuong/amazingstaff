import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task_manager_app/api/apis.dart';
import 'package:task_manager_app/models/ticket.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';
import 'package:task_manager_app/utils/exception_handler.dart';

import '../../../../utils/constants.dart';

class TaskDataProvider {
  List<TaskModel> tasks = [];


  TaskDataProvider(

      );

  Future<List<TaskModel>> getTasks() async {
    print('Có chạy getTasks nha');
    try {
      List<TaskModel> savedTasks = await fetchTicket() ;
      print(savedTasks.length);
      if (savedTasks != null) {
        tasks = savedTasks ;
        tasks.sort((a, b) {
          if (a.ticketStatusId == b.ticketStatusId) {
            return 0;
          } else if (a.ticketStatusId == 2) {
            return 1;
          } else {
            return -1;
          }
        });
      }
      return tasks;
    }catch(e){
      print('Có lỗi trong quá trình getTasks');
      throw Exception(handleException(e));
    }
  }

  Future<List<TaskModel>> sortTasks(int sortOption) async {
    switch (sortOption) {
      case 0:
        tasks.sort((a, b) {
          // Sort by date
          if ( DateTime.parse(a.createdDate!).isAfter( DateTime.parse(b.createdDate!))) {
            return -1;
          } else if ( DateTime.parse(a.createdDate!).isBefore( DateTime.parse(b.createdDate!))) {
            return 1;
          }
          return 0;
        });
        break;
      case 1:
        //sort by completed tasks
        tasks.sort((a, b) {
          if (!(a.ticketStatusId=='3') && (b.ticketStatusId=='3')) {
            return 1;
          } else if ((a.ticketStatusId=='3')&& !(b.ticketStatusId=='3')) {
            return -1;
          }
          return 0;
        });
        break;
      case 2:
      //sort by pending tasks
        tasks.sort((a, b) {
          if ((a.ticketStatusId=='3') == (b.ticketStatusId=='3')) {
            return 0;
          } else if ((a.ticketStatusId=='3')) {
            return 1;
          } else {
            return -1;
          }
        });
        break;
    }
    return tasks;
  }

  Future<List<TaskModel>> updateTask(Ticket taskModel) async {
    try {
        await changeStatus(taskModel);
        tasks = [];
        tasks = await getTasks();
      return tasks;
    } catch (exception) {
      throw Exception(handleException(exception));
    }
  }

  // Future<List<TaskModel>> deleteTask(TaskModel taskModel) async {
  //   try {
  //     tasks.remove(taskModel);
  //     final List<String> taskJsonList = tasks.map((task) =>
  //         json.encode(task.toJson())).toList();
  //     prefs!.setStringList(Constants.taskKey, taskJsonList);
  //     return tasks;
  //   } catch (exception) {
  //     throw Exception(handleException(exception));
  //   }
  // }

  Future<List<TaskModel>> searchTasks(String keywords) async {
    var searchText = keywords.toLowerCase();
    List<TaskModel> matchedTasked = tasks;
    return matchedTasked.where((task) {
      final titleMatches = task.code!.toLowerCase().contains(searchText);
      final descriptionMatches = task.title!.toLowerCase().contains(searchText);
      return titleMatches || descriptionMatches;
    }).toList();
  }
}
