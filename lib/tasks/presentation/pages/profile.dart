import 'package:flutter/material.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';
import 'package:task_manager_app/routes/pages.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';
class ProfileScreen extends StatefulWidget {

  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title:Stack(
            children: [
              Container(
                  child: Icon(Icons.arrow_back)),
              SizedBox(width: 40,),
              Container(
                  alignment: Alignment.bottomRight,
                  child: Icon(Icons.settings))
            ],
          ) ,
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
                // height: MediaQuery.of(context).size.height ,
                // width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    SizedBox(
                      width: 120,
                      height: 120,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image(
                          image: AssetImage(
                            'assets/images/logo.png'
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Text('Staff Name'),
                    Text('staff@gmail.com',),
                    SizedBox(height: 30,),
                    SizedBox(
                      width: 200,
                      child: ElevatedButton(
                        onPressed: (){},
                        child: Text(
                          'Edit Profile',
                          style: TextStyle(
                            fontSize: 18
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 40,),
                    Divider(
                      endIndent: 30,
                      indent: 30,
                    ),
                    SizedBox(height: 40,),

                    Container(
                      width: 350,
                      child: ListTile(
                        title: Text('Thông Tin Tài Khoản',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                            fontSize: 20,
                            fontWeight: FontWeight.w700
                          ),
                        ),
                        trailing: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.arrow_right, size: 18,color:Colors.grey),
                        ),
                        leading: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.settings,
                          size: 18,
                          color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      width: 350,
                      child: ListTile(
                        title: Text('Thông Báo',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 20,
                              fontWeight: FontWeight.w700)),
                        trailing: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.arrow_right, size: 18,color:Colors.grey),
                        ),
                        leading: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.settings,
                            size: 18,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      width: 350,
                      child: ListTile(
                        title: Text('Cài Đặt',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 20,
                              fontWeight: FontWeight.w700)),
                        trailing: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.arrow_right, size: 18,color:Colors.grey),
                        ),
                        leading: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.settings,
                            size: 18,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      width: 350,
                      child: ListTile(
                        title: Text('Trợ Giúp',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 20,
                              fontWeight: FontWeight.w700)),
                        trailing: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.arrow_right, size: 18,color:Colors.grey),
                        ),
                        leading: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey.withOpacity(0.1),
                          ),
                          child: Icon(Icons.settings,
                            size: 18,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    InkWell(
                      onTap: (){
                        Navigator.pushNamedAndRemoveUntil(
                          context,
                          Pages.login,
                              (route) => false,
                        );
                      },
                      child: Container(
                        width: 350,
                        child: ListTile(
                          title: Text('Thoát',
                            style: TextStyle(
                                 color: Colors.red.shade700,
                                fontSize: 20,
                                fontWeight: FontWeight.w700)),
                          leading: Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Colors.grey.withOpacity(0.1),
                            ),
                            child: Icon(Icons.settings,
                              size: 18,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
            ),
          ),
        ),
        bottomNavigationBar:  StylishBottomBar(
          option: DotBarOptions(
            dotStyle: DotStyle.tile,
            gradient: const LinearGradient(
              colors: [
                Colors.deepPurple,
                Colors.pink,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          items: [
            BottomBarItem(
              icon: const Icon(
                Icons.house_outlined,
              ),
              selectedIcon: const Icon(Icons.house_rounded),
              selectedColor: Colors.teal,
              unSelectedColor: Colors.grey,
              title: const Text('Home'),
              showBadge: false,
              badgeColor: Colors.purple,
              badgePadding: const EdgeInsets.only(left: 4, right: 4),
            ),
            BottomBarItem(
                icon: const Icon(
                  Icons.person_outline,
                ),
                selectedIcon: const Icon(
                  Icons.person,
                ),
                selectedColor: Colors.deepPurple,
                title: const Text('Profile')),
          ],
          hasNotch: true,
          currentIndex: 1,
          notchStyle: NotchStyle.square,
          onTap: (index) {
            if (index == 0) {
              Navigator.pushNamed(context, Pages.home);
            }
          },
        ),
      ),

    );
  }

}
