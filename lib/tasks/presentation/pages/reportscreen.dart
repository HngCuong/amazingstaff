import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';
import 'package:task_manager_app/api/apis.dart';
import 'package:task_manager_app/models/log.dart';
import 'package:task_manager_app/models/logrequest.dart';
import 'package:task_manager_app/provider/url.dart';
import 'package:task_manager_app/tasks/presentation/widget/reportcard.dart';
import 'package:path/path.dart' as Path;
class ReportScreen extends StatefulWidget {
  final int id;

  const ReportScreen({super.key, required this.id});

  @override
  State<ReportScreen> createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  List<Log> list = [];
  final TextEditingController title = TextEditingController();
  final TextEditingController content = TextEditingController();
  @override
  void initState() {
    super.initState();
    fetchLog(widget.id).then((value) => {
          setState(() {
            list = value;
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<UrlProvider>(context);

    String convertDateFormat(String originalDate) {
      DateTime dateTime = DateTime.parse(originalDate);
      String formattedDate = DateFormat('dd/MM/yyyy').format(dateTime);
      return formattedDate;
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Stack(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: Center(
                                        child: IconButton(
                                            color: Colors.black,
                                            onPressed: () {},
                                            icon: Icon(
                                              Icons.arrow_back,
                                              color: Colors.black,
                                              size: 27,
                                            )))),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Center(
                                    child: Text(
                                      "Báo Cáo Tiến Độ",
                                      style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height - 150.6,
                        decoration: BoxDecoration(
                          color: Colors.white, // Màu nền của container
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(left: 15),
                                        child: Text(
                                          "Tiêu đề báo cáo",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Text(
                                        " *",
                                        style: TextStyle(color: Colors.red),
                                      )
                                    ],
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8.0),
                                  //   child: Container(
                                  //     height: 30,
                                  //     width: 50,
                                  //     decoration: BoxDecoration(
                                  //         color: Colors.grey.shade100,
                                  //         border: Border.all(
                                  //           color: Colors.blue.shade400, // Màu của viền
                                  //         ),
                                  //         borderRadius: BorderRadius.all(Radius.circular(10))
                                  //     ),
                                  //     margin: EdgeInsets.only(right: 20),
                                  //     child: Container(
                                  //         alignment: Alignment.center,
                                  //         child: Text("Chọn", textAlign: TextAlign.center,
                                  //           style: TextStyle(color: Colors.black38),
                                  //         )),
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            TextFormField(
                              controller: title,
                              maxLines: 1,
                              decoration: InputDecoration(
                                hintText: 'Tiêu đề báo cáo',
                                hintStyle: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.grey,
                                ),
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15.0),
                                // Độ cao của TextFormField
                                border: InputBorder.none, // Tắt viền
                              ),
                            ),
                            Center(
                              child: Container(
                                width: 300,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black26),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 15),
                                      child: Text(
                                        "Nội dung báo cáo",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Text(
                                      " *",
                                      style: TextStyle(color: Colors.red),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            TextFormField(
                              controller: content,
                              maxLines: 2,
                              decoration: InputDecoration(
                                hintText: 'Nội dung báo cáo',
                                hintStyle: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.grey,
                                ),
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15.0),
                                // Độ cao của TextFormField
                                border: InputBorder.none, // Tắt viền
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12))),
                              child: DottedBorder(
                                color: Colors.white,
                                borderType: BorderType.RRect,
                                radius: Radius.circular(12),
                                padding: EdgeInsets.all(6),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                  child: Center(
                                    child: Container(
                                      color: Colors.red,
                                      height: 200,
                                      width: MediaQuery.of(context).size.width,
                                      child: ImagePickerExample(),
                                      // child: Container(
                                      //   alignment: Alignment.center,
                                      //   child: Text("Thêm ảnh", textAlign: TextAlign.center),
                                      // ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, bottom: 10),
                              child: Text(
                                'Chi Tiết Tiến Độ',
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              color: Colors.white,
                              height: 200,
                              child: SingleChildScrollView(
                                  child: Column(
                                children: [
                                  Container(
                                      height: 200,
                                      child: ListView.builder(
                                        itemCount: list.length,
                                        itemBuilder: (context, index) =>
                                            ReportCard(
                                          date: convertDateFormat(list[index].createdDate!),
                                          note: list[index].note!,
                                        ),
                                      ))
                                ],
                              )),
                            ),
                            Center(
                              child: Container(
                                width: 300,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black26),
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 5),
                                height: 60,
                                width: 150,
                                child: Center(
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      print('có nhấn nút');
                                        LogRequest log = new LogRequest(
                                          note: title.value.text +'\n'+ content.value.text,
                                          createdDate: DateTime.now().toString(),
                                          code: 'T',
                                          imgUrl: provider.url,
                                          ticketId: widget.id,
                                        );
                                       if( await createLog(log)){
                                          print('Đã gửi thành công');
                                          final snackBar = SnackBar(
                                            backgroundColor: Colors.green,
                                            content: Text('Báo cáo đã gửi thành công'),
                                            duration: Duration(seconds: 2), // Duration for which snackbar is visible
                                            action: SnackBarAction(
                                              label: 'Success',
                                              onPressed: () {
                                                // Code to execute when the user presses the "Undo" action
                                                print('Undo action pressed');
                                              },
                                            ),
                                          );
                                          // Show the snackbar
                                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                       }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white, // Màu nền của nút
                                      onPrimary: Colors.white, // Màu chữ của nút
                                      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30.0), // Bo tròn góc
                                      ),
                                      elevation: 3, // Độ nổi của nút
                                    ),
                                    child: Text(
                                      'Báo cáo',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black
                                      ),
                                    ),
                                  )
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )),
        ),
        bottomNavigationBar: StylishBottomBar(
          option: DotBarOptions(
            dotStyle: DotStyle.tile,
            gradient: const LinearGradient(
              colors: [
                Colors.deepPurple,
                Colors.pink,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          items: [
            BottomBarItem(
              icon: const Icon(
                Icons.house_outlined,
              ),
              selectedIcon: const Icon(Icons.house_rounded),
              selectedColor: Colors.teal,
              unSelectedColor: Colors.grey,
              title: const Text('Home'),
              badge: const Text('9+'),
              showBadge: true,
              badgeColor: Colors.purple,
              badgePadding: const EdgeInsets.only(left: 4, right: 4),
            ),
            BottomBarItem(
                icon: const Icon(
                  Icons.person_outline,
                ),
                selectedIcon: const Icon(
                  Icons.person,
                ),
                selectedColor: Colors.deepPurple,
                title: const Text('Profile')),
          ],
          hasNotch: true,
          currentIndex: 1,
          notchStyle: NotchStyle.square,
          onTap: (index) {
            // if (index == selected) return;
            // controller.jumpToPage(index);
            // setState(() {
            //   selected = index;
            // });
          },
        ),
      ),
    );
  }
}

class ImagePickerExample extends StatefulWidget {
  @override
  _ImagePickerExampleState createState() => _ImagePickerExampleState();
}

class _ImagePickerExampleState extends State<ImagePickerExample> {
  XFile? _imageFile;

  // Hàm mở thư viện ảnh và chọn ảnh
  Future<void> _pickImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

// Hàm tải ảnh lên Firebase Storage và in đường dẫn
  Future<void> _uploadImageToFirebase() async {
    print('Có chạy');
    if (_imageFile == null) return;
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('images/${Path.basename(_imageFile!.path)}');
    UploadTask uploadTask = storageReference.putFile(File(_imageFile!.path));
    await uploadTask.whenComplete(() async {
      String downloadURL = await storageReference.getDownloadURL();
      final provider = Provider.of<UrlProvider>(context, listen: false);
      provider.saveUrl(downloadURL);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _imageFile == null
                ? Text('Không có ảnh được chọn')
                : Container(
                height: 130,
                width: MediaQuery.of(context).size.width,
                child: Image.file(File(_imageFile!.path))),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: (){
                _pickImage().then((value) => _uploadImageToFirebase()
                );
              },
              child: Text('Chọn ảnh'),
            ),
          ],
        ),
      ),
    );
  }
}