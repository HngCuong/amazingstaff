import 'package:flutter/material.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';
import 'package:url_launcher/url_launcher.dart';
class DetailScreen extends StatefulWidget {
  final TaskModel taskModel;
  const DetailScreen({super.key, required this.taskModel});

  @override
  State<DetailScreen> createState() => _ReportScreenState();
}

class _ReportScreenState extends State<DetailScreen> {
  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Container(
              height: MediaQuery.of(context).size.height ,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        height: 80,
                        width: 80,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(
                                  'assets/images/task.png',
                              )
                            )
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(widget.taskModel.title!,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                            ),
                            Text(widget.taskModel.levelId!,
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.grey.shade500
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color:  Color(0xFFF8D082)
                              ),
                              padding: EdgeInsets.all(7),
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(3),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white10
                                    ),
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.white,
                                      size: 15,
                                    ),
                                  ),
                                  Text(widget.taskModel.ticketStatusId!.toString(), style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.w600
                                  ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Divider(color: Colors.grey.shade300,),
                  Stack(
                    children: [
                      Center(
                        child: Container(
                          height: 120,
                          width: 360,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/banner.jpg'
                              ),
                              fit: BoxFit.cover, // Cách hình ảnh sẽ được điều chỉnh để vừa với Box
                            ),
                            borderRadius: BorderRadius.circular(18),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 25,vertical: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            // CircleAvatar(
                            //   child: Icon( Icons.add,
                            //   color: Colors.grey,
                            //   size: 27,
                            //   ),
                            //   maxRadius: 25,
                            //   backgroundColor: Colors.white,
                            // ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Text(
                                //   "You \' got a new award",
                                //   style: TextStyle(
                                //     fontWeight: FontWeight.bold,
                                //     color: Colors.white
                                //   ),
                                // ),
                                // Text(
                                //   "You \' got a new award",
                                //   style: TextStyle(
                                //       fontWeight: FontWeight.bold,
                                //       color: Colors.white.withOpacity(0.9),
                                //     fontSize: 16,
                                //   ),
                                // )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Text('Thông Tin Khách Hàng',style: TextStyle(
                      fontSize: 16,fontWeight: FontWeight.bold
                    )
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Text("#KH20240",
                          style: TextStyle(
                              fontSize: 35,
                              color: Colors.black,
                              fontWeight: FontWeight.w600
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Tên Khách Hàng",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Text("Địa Chỉ",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(widget.taskModel.customerId!.name!,
                                    style: TextStyle(
                                        fontSize: 16
                                    ),
                                  ),
                                  Text(widget.taskModel.customerId!.address!,
                                    style: TextStyle(
                                        fontSize: 16,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Email",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Text("Số Điện Thoại",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(widget.taskModel.customerId!.email!,
                                    style: TextStyle(
                                        fontSize: 16
                                    ),
                                  ),
                                  InkWell(
                                    onTap: (){
                                      _makePhoneCall(widget.taskModel.customerId!.phoneNumber!);
                                    },
                                    child: Text(widget.taskModel.customerId!.phoneNumber!,
                                      style: TextStyle(
                                          fontSize: 16
                                      ),

                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Hạn Hợp Đồng",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text('',
                                    style: TextStyle(
                                        fontSize: 16
                                    ),

                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(widget.taskModel.customerId!.status!,
                                    style: TextStyle(
                                        fontSize: 16
                                    ),
                                  ),
                                  Text('',
                                    style: TextStyle(
                                        fontSize: 16
                                    ),

                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(color: Colors.grey.shade300,),
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text('Ghi Chú',style: TextStyle(
                          fontSize: 20,fontWeight: FontWeight.bold
                      )
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.all(20),
                      height: 140,
                      width: 400,
                      child: Text(widget.taskModel.note!,
                      style: TextStyle(
                        fontSize: 16
                      ),
                      ),
                    ),
                  )
                ],
              )
          ),
        ),
        bottomNavigationBar:  StylishBottomBar(
          option: DotBarOptions(
            dotStyle: DotStyle.tile,
            gradient: const LinearGradient(
              colors: [
                Colors.deepPurple,
                Colors.pink,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          items: [

            BottomBarItem(
              icon: const Icon(
                Icons.house_outlined,
              ),
              selectedIcon: const Icon(Icons.house_rounded),
              selectedColor: Colors.teal,
              unSelectedColor: Colors.grey,
              title: const Text('Home'),
              badge: const Text('9+'),
              showBadge: true,
              badgeColor: Colors.purple,
              badgePadding: const EdgeInsets.only(left: 4, right: 4),
            ),

            BottomBarItem(
                icon: const Icon(
                  Icons.person_outline,
                ),
                selectedIcon: const Icon(
                  Icons.person,
                ),
                selectedColor: Colors.deepPurple,
                title: const Text('Profile')),

          ],
          hasNotch: true,
          currentIndex: 1,
          notchStyle: NotchStyle.square,
          onTap: (index) {
            // if (index == selected) return;
            // controller.jumpToPage(index);
            // setState(() {
            //   selected = index;
            // });
          },
        ),
      ),

    );
  }

}
