import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:task_manager_app/models/ticket.dart';
import 'package:task_manager_app/tasks/data/local/model/task_model.dart';
import 'package:task_manager_app/tasks/presentation/bloc/tasks_bloc.dart';
class Alert extends StatefulWidget {
  final TaskModel taskModel;
  const Alert({super.key, required this.taskModel});

  @override
  State<Alert> createState() => _AlertState();
}

class _AlertState extends State<Alert> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 32,
          vertical: 16
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset("assets/images/logo.png",
              width: 72,
            ),
            SizedBox(height: 24,),
            Text("Alert",
            style: GoogleFonts.montserrat(
              fontSize: 24,
              color: Colors.red,
              fontWeight: FontWeight.bold
            ),
            ),
            SizedBox(height: 4,),
            Text("Bạn có chắc là bạn muốn xác nhận đã hoàn thành không",
            textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.black
              ),
            ),
            SizedBox(height: 32,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 32
                      ),
                      foregroundColor: Colors.red,
                      side: BorderSide(
                        color: Colors.red
                      )
                    ),
                    onPressed: (){
                      Navigator.of(context).pop(false);
                    },
                    child: Text("Cancel")
                ),
                ElevatedButton(onPressed: (){
                  Ticket ticket = new Ticket(
                    id: widget.taskModel.id,
                    code: widget.taskModel.code,
                    title: widget.taskModel.title,
                    branchId: widget.taskModel.branchId,
                    createdDate: widget.taskModel.createdDate,
                    customerId: widget.taskModel.customerId!.id,
                    levelId: 1,
                    note: widget.taskModel.note,
                    ticketStatusId: 3,
                    ticketTypeId: 1,
                    assignedUserId: widget.taskModel.assignedUserId,
                    createdBy: widget.taskModel.createdBy
                  );
                  context.read<TasksBloc>().add(
                      UpdateTaskEvent(taskModel: ticket));
                  context.read<TasksBloc>().add(FetchTaskEvent());

                  Navigator.of(context).pop(true);
                },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      foregroundColor: Colors.black,
                      padding: EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 40
                      )
                    )
                    ,child: Text("Yes",style: TextStyle(
                      color: Colors.white
                    ),))
              ],
            )
          ],
        ),
      ),
    );
  }
}
