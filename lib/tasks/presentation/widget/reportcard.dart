import 'package:flutter/material.dart';
class ReportCard extends StatefulWidget {
  final String date;
  final String note;
  const ReportCard({super.key, required this.date, required this.note});

  @override
  State<ReportCard> createState() => _ReportCardState();
}

class _ReportCardState extends State<ReportCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Container(
            height: 60,
            width: 100,
            child: Text(widget.date),
          ),
          Container(
            height: 60,
            width: 280,
            child: Text(widget.note,maxLines: 3),
          )
        ],
      ),
    );
  }
}
